using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIActivator : MonoBehaviour
{
    [SerializeField] private GameObject _authorizationCanvas;
    [SerializeField] private GameObject _registrationCanvas;

    private void Start()
    {
        ShowAuthCanvas();
    }

    public void ShowAuthCanvas()
    {
        _registrationCanvas.SetActive(false);
        _authorizationCanvas.SetActive(true);
    }
    public void ShowRegisterCanvas()
    {
        _authorizationCanvas.SetActive(false);
        _registrationCanvas.SetActive(true);
    }

}
