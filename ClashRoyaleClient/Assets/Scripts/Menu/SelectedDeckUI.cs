using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Menu
{
    public class SelectedDeckUI : MonoBehaviour
    {
        [SerializeField] private Image[] _images;
        [SerializeField] private DeckManager _deckManager;

        public void UpdateCardList(IReadOnlyList<Card> cards)
        {
            for (int i = 0; i < _images.Length; i++)
            {
                if (i < cards.Count)
                {
                    _images[i].sprite = cards[i].Sprite;
                    _images[i].enabled = true;
                }
                else
                {
                    _images[i].sprite = null;
                    _images[i].enabled = false;
                }
            }
        }
    }
}
