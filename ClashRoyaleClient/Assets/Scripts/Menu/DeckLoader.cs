using System;
using System.Collections.Generic;
using Networks;
using UnityEngine;
using Network = Networks.Network;

namespace Menu
{
    [Serializable]
    public class DeckLoader : MonoBehaviour
    {
        [SerializeField] private DeckManager _deckManager;
        [SerializeField] private List<int> _availableCardsIndexes = new();
        [SerializeField] private int[] _selectedCardsIndexes = new int[5];

        private void Start()
        {
            Load();
        }

        private void Load()
        {
            Network.Instance.Post(URLLibrary.MAIN + URLLibrary.GETDECKINFO, UserIDField, OnSuccessLoad, OnErrorLoad);
        }

        private void OnSuccessLoad(string data)
        {
            DeckData deckData = JsonUtility.FromJson<DeckData>(data);

            _selectedCardsIndexes = new int[deckData.selectedIDs.Length];
            for (int i = 0; i < _selectedCardsIndexes.Length; i++)
                int.TryParse(deckData.selectedIDs[i], out _selectedCardsIndexes[i]);

            for (int i = 0; i < deckData.availableCards.Length; i++)
            {
                int.TryParse(deckData.availableCards[i].id, out int id);
                _availableCardsIndexes.Add(id);
            }
            
            _deckManager.Init(_availableCardsIndexes, _selectedCardsIndexes);
        }

        private void OnErrorLoad(string error)
        {
            Debug.LogError(error);
            Load();
        }

        private static Dictionary<string, string> UserIDField =>
            new() { { "userID", UserInfo.Instance.ID.ToString() } };
    }
}