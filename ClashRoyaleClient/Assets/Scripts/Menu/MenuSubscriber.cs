using System;
using UnityEngine;

namespace Menu
{
    public class MenuSubscriber : MonoBehaviour
    {
        [SerializeField] private DeckManager _deckManager;
        [SerializeField] private SelectedDeckUI _selectedDeckUI;
        [SerializeField] private SelectedDeckUI _selectedDeckUI2;
        [SerializeField] private SelectedDeckUI _selectedDeckUIMatchmaking;

        [SerializeField] private AvailableDeckUI _availableDeckUI;
        

        private void Start()
        {
            _deckManager.OnSelectedUpdate += _selectedDeckUI.UpdateCardList;
            _deckManager.OnSelectedUpdate += _selectedDeckUI2.UpdateCardList;
            _deckManager.OnSelectedUpdate += _selectedDeckUIMatchmaking.UpdateCardList;
            _deckManager.OnAvailableUpdate += _availableDeckUI.UpdateCardList;
        }

        private void OnDestroy()
        {
            _deckManager.OnSelectedUpdate -= _selectedDeckUI.UpdateCardList;
            _deckManager.OnSelectedUpdate -= _selectedDeckUI2.UpdateCardList;
            _deckManager.OnSelectedUpdate -= _selectedDeckUIMatchmaking.UpdateCardList;
            _deckManager.OnAvailableUpdate -= _availableDeckUI.UpdateCardList;
        }
    }
}