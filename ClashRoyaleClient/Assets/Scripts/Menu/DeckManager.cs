using System;
using System.Collections.Generic;
using Networks;
using UnityEditor.PackageManager;
using UnityEngine;
using Network = Networks.Network;

namespace Menu
{
    public class DeckManager : MonoBehaviour
    {
        private const string url = URLLibrary.MAIN + URLLibrary.SETSELECTDECK;
        public event Action<IReadOnlyList<Card>, IReadOnlyList<Card>> OnAvailableUpdate;
        public event Action<IReadOnlyList<Card>> OnSelectedUpdate;

        public IReadOnlyList<Card> AvailableCards => _availableCards;
        public IReadOnlyList<Card> SelectedCards => _selectedCards;

        [SerializeField] private GameObject _lockCanvas;
        
        [SerializeField] private Card[] _cards;
        [SerializeField] private List<Card> _availableCards = new();
        [SerializeField] private List<Card> _selectedCards = new();

        public void Init(List<int> availableCardIndexes, int[] selectedCardIndexes)
        {
            foreach (int index in availableCardIndexes)
                _availableCards.Add(_cards[index]);

            foreach (int index in selectedCardIndexes)
                _selectedCards.Add(_cards[index]);

            OnAvailableUpdate?.Invoke(AvailableCards, SelectedCards);
            OnSelectedUpdate?.Invoke(SelectedCards);
            
            HideLockScreen();
        }

        private void HideLockScreen()
        {
            _lockCanvas.SetActive(false);
        }

        public void ChangeDeck(IReadOnlyList<Card> selectedCards, Action success)
        {
            ShowLockScreen();
            int[] IDs = new int[selectedCards.Count];
            for (int i = 0; i < selectedCards.Count; i++)
            {
                IDs[i] = selectedCards[i].ID;
            }

            string json = JsonUtility.ToJson(new IDsWrapper(IDs));
            Dictionary<string, string> data = new Dictionary<string, string>
                { { "userID", UserInfo.Instance.ID.ToString() }, { "json", json } };

            success += () =>
            {
                for (int i = 0; i < _selectedCards.Count; i++)
                {
                    _selectedCards[i] = selectedCards[i];
                }
                OnSelectedUpdate?.Invoke(SelectedCards);
            };
            Network.Instance.Post(url, data, (s) => OnSuccess(s, success), OnError);
        }

        private void ShowLockScreen()
        {
            _lockCanvas.SetActive(true);
        }

        private void OnSuccess(string obj, Action success)
        {
            if (obj != "ok")
            {
                OnError(obj);
                return;
            }

            success?.Invoke();
            HideLockScreen();
        }

        private void OnError(string obj)
        {
            Debug.LogError("Неудачная попытка отправки новой колоды: " + obj);
            HideLockScreen();
        }


        #region Editor

        [SerializeField] private AvailableDeckUI _availableDeckUI;

#if UNITY_EDITOR
        private void OnValidate()
        {
            _availableDeckUI.SetAllCardsCount(_cards);
        }
#endif

        #endregion
    }
}