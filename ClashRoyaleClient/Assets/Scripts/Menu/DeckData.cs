using System;

namespace Menu
{
    [Serializable]
    public class DeckData
    {
        public Availablecard[] availableCards;
        public string[] selectedIDs;
    }
}