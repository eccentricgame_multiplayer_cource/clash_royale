using UnityEngine;

namespace Menu
{
    public class CardToggle : MonoBehaviour
    {
        [SerializeField] private CardSelector _selector;
        [SerializeField] private int _index;

        public void Click(bool value)
        {
            if (value)
            {
                _selector.SetSelectIndex(_index);
            }
        }
    }
}