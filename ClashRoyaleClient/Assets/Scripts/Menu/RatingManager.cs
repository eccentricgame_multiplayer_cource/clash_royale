using System;
using System.Collections.Generic;
using Networks;
using UnityEditor.PackageManager;
using UnityEngine;
using UnityEngine.UI;
using Network = Networks.Network;

namespace Menu
{
    public class RatingManager : MonoBehaviour
    {
        [SerializeField] private Text _ratingText;

        private static string url => URLLibrary.MAIN + URLLibrary.GETRATING;

        private void Start()
        {
            InitRating();
        }

        private void InitRating()
        {
            Dictionary<string, string> data = new Dictionary<string, string>()
            {
                {
                    "userId", UserInfo.Instance.ID.ToString()
                }
            };
            Network.Instance.Post(url, data, OnSuccess, OnError);
        }

        private void OnError(string obj)
        {
            Debug.LogError(obj);
        }

        private void OnSuccess(string obj)
        {
            string[] result = obj.Split('|');
            if (result.Length != 3)
            {
                OnError("Длина массива != 3" + obj);
                return;
            }

            if (result[0] != "ok")
            {
                OnError("Странный результат" + obj);
                return;
            }

            _ratingText.text = $"<color=green>{result[1]}</color> : <color=red>{result[2]}</color>";
        }
    }
}