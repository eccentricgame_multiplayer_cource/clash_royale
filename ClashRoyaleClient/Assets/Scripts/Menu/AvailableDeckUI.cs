using System.Collections.Generic;
using UnityEngine;

namespace Menu
{
    public class AvailableDeckUI : MonoBehaviour
    {
        [SerializeField] private CardSelector _cardSelector;
        [SerializeField] private List<AvailableCardUI> _availableCardUI = new();

        private static AvailableCardUI.CardStateType Available => AvailableCardUI.CardStateType.Available;
        private static AvailableCardUI.CardStateType Locked => AvailableCardUI.CardStateType.Locked;
        private static AvailableCardUI.CardStateType Selected => AvailableCardUI.CardStateType.Selected;

        #region Editor

#if UNITY_EDITOR

        [SerializeField] private AvailableCardUI _availableCardUIPrefab;
        [SerializeField] private Transform _parentTransform;


        public void SetAllCardsCount(Card[] cards)
        {
            DestroyAllCardsUI();

            CreateNewCardsUI(cards);

            UnityEditor.EditorUtility.SetDirty(this);
        }

        private void CreateNewCardsUI(Card[] cards)
        {
            for (var index = 1; index < cards.Length; index++)
            {
                var card = cards[index];
                AvailableCardUI cardUI = Instantiate(_availableCardUIPrefab, _parentTransform);
                cardUI.Create(_cardSelector, card, index);
                _availableCardUI.Add(cardUI);
            }
        }

        private void DestroyAllCardsUI()
        {
            foreach (AvailableCardUI cardUI in _availableCardUI)
            {
                GameObject go = cardUI.gameObject;
                UnityEditor.EditorApplication.delayCall += () => DestroyImmediate(go);
            }

            _availableCardUI.Clear();
        }

#endif

        #endregion

        public void UpdateCardList(IReadOnlyList<Card> available, IReadOnlyList<Card> selected)
        {
            foreach (AvailableCardUI cardUI in _availableCardUI) 
                cardUI.SetState(Locked);

            foreach (var card in available) 
                _availableCardUI[card.ID - 1].SetState(Available);

            foreach (Card card in selected) 
                _availableCardUI[card.ID - 1].SetState(Selected);
        }
    }
}