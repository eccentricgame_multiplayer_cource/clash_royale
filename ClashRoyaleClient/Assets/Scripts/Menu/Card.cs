using System;
using UnityEngine;

namespace Menu
{
    [Serializable]
    public class Card
    {
        public string Name => _name;
        public Sprite Sprite => _sprite;
        public int ID => _id;

        [SerializeField] private string _name;
        [SerializeField] private int _id;
        [SerializeField] private Sprite _sprite;
    }
}