using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Menu
{
    public class CardSelector : MonoBehaviour
    {
        public IReadOnlyList<Card> AvailableCards => _availableCards;
        public IReadOnlyList<Card> SelectedCards => _selectedCards;

        private int _selectToggleIndex = 0;
        [SerializeField] private DeckManager _deckManager;

        [SerializeField] private List<Card> _selectedCards = new();
        private List<Card> _availableCards = new();

        [SerializeField] private AvailableDeckUI _availableDeckUI;
        [SerializeField] private SelectedDeckUI _selectedDeckUI;
        
        [Space(24)]
        [SerializeField] private GameObject _mainCanvas;
        [SerializeField] private GameObject _cardSelectCanvas;


        private void OnEnable()
        {
            SetupAvailableList();
            SetupSelectedList();
        }

        public void SaveChanges()
        {
            _deckManager.ChangeDeck(_selectedCards, CloseChangesWindow);
        }

        public void CancelChanges()
        {
            SetupAvailableList();
            SetupSelectedList();
            UpdateUI();
            CloseChangesWindow();
        }
        

        public void SetSelectIndex(int selectToggleIndex) => 
            _selectToggleIndex = selectToggleIndex;

        public void SelectCard(int cardId)
        {
            _selectedCards[_selectToggleIndex] = _availableCards[cardId - 1];
            UpdateUI();
        }

        public void CloseChangesWindow()
        {
            _mainCanvas.SetActive(true);
            _cardSelectCanvas.SetActive(false);
        }

        private void SetupAvailableList()
        {
            _availableCards.Clear();
            foreach (Card card in _deckManager.AvailableCards)
                _availableCards.Add(card);
        }

        private void SetupSelectedList()
        {
            _selectedCards.Clear();
            foreach (Card card in _deckManager.SelectedCards)
                _selectedCards.Add(card);
        }

        private void UpdateUI()
        {
            _selectedDeckUI.UpdateCardList(SelectedCards);
            _availableDeckUI.UpdateCardList(AvailableCards, SelectedCards);
        }
    }
}