using System;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace Menu
{
    public class AvailableCardUI : MonoBehaviour
    {
        [SerializeField] private Text _text;
        [SerializeField] private Color _availableColor;
        [SerializeField] private Color _selectedColor;
        [SerializeField] private Color _lockedColor;

        [SerializeField] private CardSelector _cardSelector;
        [SerializeField] private int _id;
        

        public void Click()
        {
            switch (_currentState)
            {
                case CardStateType.None:
                    break;
                case CardStateType.Available:
                    _cardSelector.SelectCard(_id-1);
                    SetState(CardStateType.Selected);
                    break;
                case CardStateType.Selected:
                    break;
                case CardStateType.Locked:
                    break;
            }
        }

        public void SetState(CardStateType state)
        {
            _currentState = state;
            UpdateTextColor();
        }

        private void UpdateTextColor()
        {
            switch (_currentState)
            {
                case CardStateType.None:
                    break;
                case CardStateType.Available:
                    SetColor(_availableColor);
                    break;
                case CardStateType.Selected:
                    SetColor(_selectedColor);
                    break;
                case CardStateType.Locked:
                    SetColor(_lockedColor);
                    break;
            }
        }

        private void SetColor(Color availableColor)
        {
            _text.color = availableColor;
        }

        public enum CardStateType
        {
            None = 0,
            Available = 1,
            Selected = 2,
            Locked = 3
        }

        #region Editor

#if UNITY_EDITOR
        [SerializeField] private Image _image;
        private CardStateType _currentState;

        public void Create(CardSelector cardSelector, Card card, int id)
        {
            _cardSelector = cardSelector;
            _id = id;
            _image.sprite = card.Sprite;
            _text.text = card.Name;
            EditorUtility.SetDirty(this);
        }
#endif

        #endregion
    }
}