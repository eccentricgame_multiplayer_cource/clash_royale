using System;

namespace Menu
{
    [Serializable]
    public class IDsWrapper
    {
        public int[] IDs;

        public IDsWrapper(int[] ids)
        {
            IDs = ids;
        }
    }
}