using UnityEngine;

namespace MultiplayerScripts
{
    public class MatchmakingManager : MonoBehaviour
    {
        [SerializeField] private GameObject _mainMenuCanvas;
        [SerializeField] private GameObject _matchMakingCanvas;
        [SerializeField] private GameObject _cancelButton;

        private static MultiplayerManager MultiplayerManager => MultiplayerManager.Instance;


        public async void FindOpponent()
        {
            Hide(_cancelButton);

            Hide(_mainMenuCanvas);
            Show(_matchMakingCanvas);

            await MultiplayerManager.Connect();
            Show(_cancelButton);
        }

        public void CanselFind()
        {
            Hide(_matchMakingCanvas);
            Show(_mainMenuCanvas);

            MultiplayerManager.Leave();
        }

        private static void Hide(GameObject go)
        {
            go.SetActive(false);
        }

        private static void Show(GameObject go)
        {
            go.SetActive(true);
        }
    }
}