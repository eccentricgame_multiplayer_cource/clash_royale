using System.Collections.Generic;
using System.Threading.Tasks;
using Colyseus;
using Networks;
using UnityEngine;

namespace MultiplayerScripts
{
    public class MultiplayerManager : ColyseusManager<MultiplayerManager>
    {
        private const string StateHandler = "state_handler";
        private ColyseusRoom<State> _room;

        protected override void Awake()
        {
            base.Awake();

            Instance.InitializeClient();
            DontDestroyOnLoad(gameObject);
        }

        public async Task Connect()
        {
            Dictionary<string, object> data = new Dictionary<string, object>
            {
                { "id", UserInfo.Instance.ID }
            };
            _room = await Instance.client.JoinOrCreate<State>(StateHandler, data);
        }

        public void Leave()
        {
            _room?.Leave();
            _room = null;
        }
    }
}