using System;
using System.Collections;
using UnityEngine;

public class Health : MonoBehaviour
{
    public event Action<float> UpdateHealth;
    public float Max => _max;
    [SerializeField] private float _max;
    [SerializeField] private float _current;

    private void Start()
    {
        _current = _max;
    }

    public void ApplyDamage(float value)
    {
        _current -= value;
        if (_current < 0)
            _current = 0;

        UpdateHealth?.Invoke(_current);
        Debug.Log($"Объект {name}: было - {_current + value}, стало - {_current}");
    }

    public void ApplyDelayDamage(float damage, float delay)
    {
        StartCoroutine(Delay(delay, () => ApplyDamage(damage)));
    }

    private IEnumerator Delay(float delay, Action callback)
    {
        yield return new WaitForSeconds(delay);
        callback?.Invoke();
    }
}