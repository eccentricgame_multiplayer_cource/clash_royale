using System.Collections.Generic;
using TowerScripts;
using UnitScripts;
using UnityEngine;

public class MapInfo : MonoBehaviour
{
    #region SingletonOneScene

    public static MapInfo Instance { get; private set; }

    private void Awake()
    {
        if (Instance)
        {
            Destroy(gameObject);
            return;
        }

        Instance = this;
    }

    private void OnDestroy()
    {
        if (Instance == this)
            Instance = null;
    }

    #endregion

    [SerializeField] private List<Tower> _enemyTowers;
    [SerializeField] private List<Tower> _playerTowers;

    [SerializeField] private List<Unit> _enemyWalkingUnits;
    [SerializeField] private List<Unit> _playerWalkingUnits;

    [SerializeField] private List<Unit> _enemyFlyUnits;
    [SerializeField] private List<Unit> _playerFlyUnits;


    private void Start()
    {
        SubscribeDestroy(_enemyTowers);
        SubscribeDestroy(_playerTowers);
        SubscribeDestroy(_enemyWalkingUnits);
        SubscribeDestroy(_playerWalkingUnits);
    }

    public void AddTower(Tower tower)
    {
        AddToList(tower.IsEnemy ? _enemyTowers : _playerTowers, tower);
    }
    
    public void AddUnit(Unit unit)
    {
        List<Unit> list;
        if (unit.Parameters.IsEnemy)
            list = unit.Parameters.IsFly ? _enemyFlyUnits : _enemyWalkingUnits;
        else
            list = unit.Parameters.IsFly ? _playerFlyUnits : _playerWalkingUnits;

        AddToList(list, unit);
    }

    public Tower GetNearestTower(in Vector3 currentPosition, bool enemy)
    {
        List<Tower> towers = enemy ? _enemyTowers : _playerTowers;
        return GetNearestTarget(currentPosition, towers, out _);
    }

    public bool TryGetNearestAnyUnit(Vector3 currentPosition, bool enemy, out Unit unit,
        out float distance) /// Переписать!!!!!!!!!!!!
    {
        TryGetNearestWalkingUnit(currentPosition, enemy, out Unit walkingUnit, out float walkingDistance);
        TryGetNearestFlyUnit(currentPosition, enemy, out Unit flyUnit, out float flyDistance);

        if (flyDistance < walkingDistance)
        {
            unit = flyUnit;
            distance = flyDistance;
        }
        else
        {
            unit = walkingUnit;
            distance = walkingDistance;
        }

        return unit;
    }

    public bool TryGetNearestWalkingUnit(Vector3 currentPosition, bool enemy, out Unit unit, out float distance)
    {
        List<Unit> units = enemy ? _enemyWalkingUnits : _playerWalkingUnits;
        unit = GetNearestTarget(currentPosition, units, out distance);
        if (unit != null)
            return true;
        return false;
    }

    public bool TryGetNearestFlyUnit(Vector3 currentPosition, bool enemy, out Unit unit, out float distance)
    {
        List<Unit> units = enemy ? _enemyFlyUnits : _playerFlyUnits;
        unit = GetNearestTarget(currentPosition, units, out distance);
        if (unit != null)
            return true;
        return false;
    }

    private void SubscribeDestroy<T>(List<T> objects) where T : IDestroyed
    {
        foreach (T obj in objects)
        {
            obj.Destroyed += RemoveAndUnSubscribe;

            void RemoveAndUnSubscribe()
            {
                RemoveFromList(objects, obj);
                obj.Destroyed -= RemoveAndUnSubscribe;
            }
        }
    }


    private void AddToList<T>(List<T> list, T obj) where T : IDestroyed
    {
        if (list.Contains(obj))
            return;

        list.Add(obj);
        obj.Destroyed += RemoveAndUnSubscribe;

        void RemoveAndUnSubscribe()
        {
            RemoveFromList(list, obj);
            obj.Destroyed -= RemoveAndUnSubscribe;
        }
    }

    private void RemoveFromList<T>(List<T> list, T obj)
    {
        if (list.Contains(obj))
            list.Remove(obj);
    }

    private T GetNearestTarget<T>(in Vector3 currentPosition, List<T> targets, out float distance)
        where T : MonoBehaviour
    {
        distance = float.MaxValue;
        if (targets.Count <= 0)
            return null;

        T nearestTarget = targets[0];
        distance = Vector3.Distance(currentPosition, nearestTarget.transform.position);

        for (var i = 1; i < targets.Count; i++)
        {
            T tmpTarget = targets[i];
            float tmpSqrDistance = Vector3.Distance(currentPosition, tmpTarget.transform.position);

            if (tmpSqrDistance < distance)
            {
                nearestTarget = tmpTarget;
                distance = tmpSqrDistance;
            }
        }

        return nearestTarget;
    }
}