using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour
{
    public float Speed => _speed;

    [SerializeField] private float _speed = 5f;
    private Vector3 _targetPosition = Vector3.zero;

    public void Init(Vector3 targetPosition)
    {
        _targetPosition = targetPosition;
        transform.LookAt(_targetPosition);
    }

    private void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, _targetPosition, _speed * Time.deltaTime);

        if (transform.position == _targetPosition)
            Destroy(gameObject);
    }
}