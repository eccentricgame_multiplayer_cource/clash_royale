using System;
using UnityEngine;

namespace TowerScripts
{
    public class Tower : MonoBehaviour, IDamaged, IDestroyed
    {
        public event Action Destroyed;
        
        public Health Health => _health;
        public float Radius => _radius;
        public bool IsEnemy => _isEnemy;

        [SerializeField] private Health _health;
        [SerializeField] private float _radius = 2f;
        [SerializeField] private bool _isEnemy;

        private void Awake()
        {
            MapInfo.Instance.AddTower(this);
        }

        private void Start() => 
            _health.UpdateHealth += CheckDestroy;

        public float GetDistance(in Vector3 point) => 
            Vector3.Distance(transform.position, point) - _radius;

        private void CheckDestroy(float currentHP)
        {
            if (currentHP > 0)
                return;

            _health.UpdateHealth -= CheckDestroy;
            Destroy(gameObject);
            
            Destroyed?.Invoke();
        }
    }
}