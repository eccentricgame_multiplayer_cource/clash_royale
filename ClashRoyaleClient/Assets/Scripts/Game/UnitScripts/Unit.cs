using System;
using UnitScripts.States;
using UnityEngine;

namespace UnitScripts
{
    [RequireComponent(typeof(UnitParameters), typeof(Health), typeof(UnitAnimations))]
    public class Unit : MonoBehaviour, IDamaged, IDestroyed
    {
        public event Action Destroyed;
        public UnitParameters Parameters => _parameters;
        public Health Health => _health;

        [SerializeField] private UnitParameters _parameters;
        [SerializeField] private Health _health;
        [SerializeField] private UnitAnimations _unitAnimations;


        [SerializeField] private UnitState _defaultStateSO;
        [SerializeField] private UnitState _chaseStateSO;
        [SerializeField] private UnitState _attackStateSO;

        [SerializeField] private UnitState _currentState;

        private UnitState _defaultState;
        private UnitState _attackState;
        private UnitState _chaseState;

        private void Awake()
        {
            MapInfo.Instance.AddUnit(this);
        }


        private void Start()
        {
            _defaultState = _defaultStateSO.Create(this);
            _chaseState = _chaseStateSO.Create(this);
            _attackState = _attackStateSO.Create(this);
            _currentState = _defaultState;

            _currentState.Init();

            _health.UpdateHealth += CheckDestroy;
            _unitAnimations.Init(_parameters.AttackDuaration);
        }

        private void Update()
        {
            _currentState.Run();
        }

        public void SetState(UnitStateType unitStateType)
        {
            _currentState.Finish();

            switch (unitStateType)
            {
                case UnitStateType.Default:
                    _currentState = _defaultState;
                    break;
                case UnitStateType.Chase:
                    _currentState = _chaseState;
                    break;
                case UnitStateType.Attack:
                    _currentState = _attackState;
                    break;
                default:
                    Debug.LogError("State is not processed" + unitStateType);
                    break;
            }

            _currentState.Init();
            _unitAnimations.SetState(unitStateType);
        }

        private void CheckDestroy(float currentHP)
        {
            if (currentHP > 0)
                return;

            _health.UpdateHealth -= CheckDestroy;
            Destroy(gameObject);

            Destroyed?.Invoke();
        }

#if UNITY_EDITOR
        [Space(24)] [SerializeField] private bool _debug = false;


        private void OnDrawGizmos()
        {
            if (!_debug)
                return;
            if (_chaseStateSO != null)
                _chaseStateSO.DebugDrawDistance(this);
        }

#endif
    }
}