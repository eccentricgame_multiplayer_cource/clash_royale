using UnityEngine;

namespace UnitScripts
{
    public class UnitParameters : MonoBehaviour
    {
        public float StartAttackDistance => _startAttackDistance + _radius;
        public float StopAttackDistance => _stopAttackDistance + _radius;

        public float StartChaseDistance => _startChaseDistance;
        public float StopChaseDistance => _stopChaseDistance;

        public float Speed => _speed;
        public float Radius => _radius;

        public bool IsEnemy => _isEnemy;
        public bool IsFly => _isFly;
        public float AttackDuaration => _attackDuaration;

        [SerializeField] private float _startAttackDistance = 1f;
        [SerializeField] private float _stopAttackDistance = 1.2f;
        [SerializeField] private float _startChaseDistance = 5f;
        [SerializeField] private float _stopChaseDistance = 7f;
        [SerializeField] private float _attackDuaration = 2f;
        
        [SerializeField] private float _radius = 1f;
        [SerializeField] private float _speed = 1f;
        [SerializeField] private bool _isEnemy = false;
        [SerializeField] private bool _isFly = false;
    }
}