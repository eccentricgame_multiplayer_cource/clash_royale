using UnitScripts.States;
using UnityEngine;

namespace UnitScripts
{
    public class UnitAnimations : MonoBehaviour
    {
        private static readonly int AttackSpeed = Animator.StringToHash("AttackSpeed");
        private readonly string _state = "State";
        [SerializeField] private Animator _animator;

        public void Init(float attackSpeed)
        {
            _animator.SetFloat(AttackSpeed, 1/attackSpeed);
        }

        public void SetState(UnitStateType unitState)
        {
            _animator.SetInteger(_state, (int)unitState);
        }
    }
}