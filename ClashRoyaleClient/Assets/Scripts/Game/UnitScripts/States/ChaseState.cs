using UnityEditor;
using UnityEngine;
using UnityEngine.AI;

namespace UnitScripts.States
{
    public abstract class ChaseState : UnitState
    {
        private NavMeshAgent _agent;
        private float DistanceToTarget => Vector3.Distance(CurrentPosition, TargetUnit.transform.position);
        private bool NoTarget => TargetUnit == null;
        private float StopChaseDistance => Unit.Parameters.StopChaseDistance;

        protected float AttackDistance = 0;
        protected bool TargetIsEnemy;
        protected MapInfo MapInfo;
        protected Unit TargetUnit;


        protected override void Construct(Unit unit)
        {
            base.Construct(unit);
            GetNavMeshAgent();
            TargetIsEnemy = !Unit.Parameters.IsEnemy;
            MapInfo = MapInfo.Instance;
        }

        public override void Init()
        {
            FindTarget();
        }

        public override void Run()
        {
            if (NoTarget)
            {
                SetDefaultState();
                return;
            }

            float distanceToTarget = DistanceToTarget;

            if (distanceToTarget > StopChaseDistance)
                SetDefaultState();
            else if (distanceToTarget <= AttackDistance)
                SetAttackState();
            else
                Chasing();
        }

        public override void Finish()
        {
            _agent.SetDestination(Unit.transform.position);
        }

        private void Chasing()
        {
            Debug.Log($"Преследует");
            _agent.SetDestination(TargetUnit.transform.position);
        }

        private void GetNavMeshAgent()
        {
            _agent = Unit.GetComponent<NavMeshAgent>();
            if (_agent == null)
                Debug.LogError($"Unit {Unit.name} don't have component NvMeshAgent");
        }

        protected abstract void FindTarget();

        
#if UNITY_EDITOR
        public override void DebugDrawDistance(Unit unit)
        {
            Handles.color = Color.red;
            Handles.DrawWireDisc(unit.transform.position, Vector3.up, unit.Parameters.StartChaseDistance);
            Handles.color = Color.yellow;
            Handles.DrawWireDisc(unit.transform.position, Vector3.up, unit.Parameters.StopChaseDistance);
        }
#endif
    }
}