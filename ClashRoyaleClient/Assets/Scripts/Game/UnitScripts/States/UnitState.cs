using UnityEngine;

namespace UnitScripts.States
{
    public abstract class UnitState : ScriptableObject
    {
        protected Unit Unit;
        protected Vector3 CurrentPosition => Unit.transform.position;

        public UnitState Create(Unit unit)
        {
            UnitState unitState = Instantiate(this);
            unitState.Construct(unit);
            return unitState;
        }

        protected virtual void Construct(Unit unit) => Unit = unit;

        protected void SetAttackState() => Unit.SetState(UnitStateType.Attack);
        protected void SetDefaultState() => Unit.SetState(UnitStateType.Default);
        protected void SetChaseState() => Unit.SetState(UnitStateType.Chase);

        public abstract void Init();
        public abstract void Finish();
        public abstract void Run();


#if UNITY_EDITOR

        public virtual void DebugDrawDistance(Unit unit)
        {
        }
#endif
    }

    public enum UnitStateType
    {
        None = 0,
        Default = 1,
        Chase = 2,
        Attack = 3
    }
    

}