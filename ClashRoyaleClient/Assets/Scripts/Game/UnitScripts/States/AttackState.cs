using UnityEngine;

namespace UnitScripts.States
{
    public abstract class AttackState : UnitState
    {
        private float DistanceToTarget => Vector3.Distance(Unit.transform.position, _target.transform.position);

        [SerializeField] private float _damage = 2f;
        private float _delay = 1f;
        private float _time = 1f;

        protected Health _target;
        protected bool _targetIsEnemy;
        protected MapInfo _mapInfo;
        protected bool _hasTarget;
        protected Vector3 _unitPosition;
        protected float _stopAttackDistance = 0;


        protected override void Construct(Unit unit)
        {
            base.Construct(unit);
            _targetIsEnemy = !Unit.Parameters.IsEnemy;
            _mapInfo = MapInfo.Instance;
            _delay = Unit.Parameters.AttackDuaration;
        }

        public override void Init()
        {
            _hasTarget = false;
            _unitPosition = Unit.transform.position;


            TryFindTarget();

            _time = 0f;
            if (_hasTarget)
                Unit.transform.LookAt(_target.transform.position);
            else
                Unit.SetState(UnitStateType.Default);
        }

        protected abstract void TryFindTarget();

        public override void Run()
        {
            if (!_target)
            {
                _hasTarget = false;
                _stopAttackDistance = 0;
                SetDefaultState();
                return;
            }

            if (Cooldown()) 
                return;


            if (DistanceToTarget > _stopAttackDistance)
            {
                SetChaseState();
                return;
            }

            ApplyDamage(_damage);
        }

        private bool Cooldown()
        {
            _time += Time.deltaTime;
            if (_time < _delay)
                return true;
            _time -= _delay;
            return false;
        }

        protected virtual void ApplyDamage(float damage)
        {
            Debug.Log($"Class: AttackState, methods: ApplyDamage is invoked");
            
            _target.ApplyDamage(damage);
        }

        public override void Finish()
        {
        }
    }
}