using UnityEngine;

namespace UnitScripts.States.OnlyTowerAttack
{
    [CreateAssetMenu(fileName = "_TowerMoveState", menuName = "UnitState/Tower/MoveState")]
    public class TowerMoveState : MoveState
    {
        private float DistanceToTargetTower => _targetTower.GetDistance(CurrentPosition);

        protected override void MovingProcess()
        {
            if (DistanceToTargetTower <= _startAttackDistance)
                SetAttackState();
        }
    }
}