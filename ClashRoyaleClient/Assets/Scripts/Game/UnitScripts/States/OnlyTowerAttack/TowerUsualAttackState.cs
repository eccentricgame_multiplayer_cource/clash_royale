using TowerScripts;
using UnityEngine;

namespace UnitScripts.States.OnlyTowerAttack
{
    [CreateAssetMenu(fileName = "_TowerUsualAttackState", menuName = "UnitState/Tower/UsualAttackState")]
    public class TowerUsualAttackState : AttackState
    {
        protected override void TryFindTarget()
        {
            TryAssignTargetAsTower();
        }

        private void TryAssignTargetAsTower()
        {
            if (_hasTarget)
                return;

            Tower nearestTower = _mapInfo.GetNearestTower(_unitPosition, _targetIsEnemy);
            if (nearestTower.GetDistance(_unitPosition) <= Unit.Parameters.StartAttackDistance)
            {
                _target = nearestTower.Health;
                _stopAttackDistance = Unit.Parameters.StopAttackDistance + nearestTower.Radius;
                _hasTarget = true;
            }
        }
    }
}