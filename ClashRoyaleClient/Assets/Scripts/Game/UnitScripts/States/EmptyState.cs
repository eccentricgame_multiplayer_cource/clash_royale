using UnityEngine;

namespace UnitScripts.States
{
    [CreateAssetMenu(fileName = "EmptyState", menuName = "UnitState/EmptyState")]
    public class EmptyState : UnitState
    {
        public override void Init()
        {
            SetDefaultState();
        }

        public override void Run()
        {
        }

        public override void Finish()
        {
            Debug.LogWarning($"Юнит {Unit.name} был в пустом состоянии, его перекинуло в дефолтное");
        }
    }
}