using UnityEngine;

namespace UnitScripts.States.Range
{
    [CreateAssetMenu(fileName = "_RangeMoveState", menuName = "UnitState/Range/MoveState")]
    public class RangeMoveState : MoveState
    {
        protected override void MovingProcess()
        {
            TryAttackTargetTower();
            TryChaseAnyUnit();
        }

        private void TryAttackTargetTower()
        {
            float distanceToTower = _targetTower.GetDistance(CurrentPosition);
            if (distanceToTower <= _startAttackDistance)
            {
                SetAttackState();
                Debug.Log($"Class: MoveState, methods: TryAttackTower is invoked");
            }
        }

        private void TryChaseAnyUnit()
        {
            bool hasNearestUnit = _mapInfo.TryGetNearestAnyUnit(
                CurrentPosition,
                _targetIsEnemy,
                out Unit nearestUnit,
                out float distance);

            if (!hasNearestUnit)
                return;

            if (distance <= _startChaseDistance)
            {
                SetChaseState();
                Debug.Log($"Class: MoveState, methods: TryAttackUnit is invoked");
            }
        }
    }
}