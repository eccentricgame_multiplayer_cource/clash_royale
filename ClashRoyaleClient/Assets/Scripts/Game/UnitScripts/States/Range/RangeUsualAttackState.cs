using TowerScripts;
using UnityEngine;

namespace UnitScripts.States.Range
{
    [CreateAssetMenu(fileName = "_RangeUsualAttackState", menuName = "UnitState/Range/UsualAttackState")]
    public class RangeUsualAttackState : AttackState
    {
        [SerializeField] private Arrow _arrow;
        private float DamageDelay => Vector3.Distance(Unit.transform.position, _target.transform.position) / _arrow.Speed;

        protected override void TryFindTarget()
        {
            TryAssignTargetAsUnit();
            TryAssignTargetAsTower();
        }

        protected override void ApplyDamage(float damage)
        {
            Debug.Log($"DamageDelay =  {DamageDelay}");
            
            _target.ApplyDelayDamage(damage, DamageDelay);
            CreateInitedArrow();
        }

        private void TryAssignTargetAsUnit()
        {
            if (_hasTarget)
                return;

            bool hasNearestUnit = _mapInfo.TryGetNearestAnyUnit(_unitPosition, _targetIsEnemy,
                out Unit nearestUnit, out float distance);

            if (hasNearestUnit && distance - nearestUnit.Parameters.Radius <= Unit.Parameters.StartAttackDistance)
            {
                _target = nearestUnit.Health;
                _stopAttackDistance = Unit.Parameters.StopAttackDistance + nearestUnit.Parameters.Radius;
                _hasTarget = true;
            }
        }

        private void TryAssignTargetAsTower()
        {
            if (_hasTarget)
                return;

            Tower nearestTower = _mapInfo.GetNearestTower(_unitPosition, _targetIsEnemy);
            if (nearestTower.GetDistance(_unitPosition) <= Unit.Parameters.StartAttackDistance)
            {
                _target = nearestTower.Health;
                _stopAttackDistance = Unit.Parameters.StopAttackDistance + nearestTower.Radius;
                _hasTarget = true;
            }
        }

        private void CreateInitedArrow()
        {
            Arrow arrow = Instantiate(_arrow, Unit.transform.position, Quaternion.identity);
            arrow.Init(_target.transform.position);
        }
    }
}