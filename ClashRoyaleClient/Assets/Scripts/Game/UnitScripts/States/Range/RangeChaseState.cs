using UnityEngine;

namespace UnitScripts.States.Range
{
    [CreateAssetMenu(fileName = "_RangeChaseState", menuName = "UnitState/Range/ChaseState")]
    public class RangeChaseState : ChaseState
    {
        protected override void FindTarget()
        {
            if (MapInfo.TryGetNearestAnyUnit(CurrentPosition, TargetIsEnemy, out TargetUnit, out float distance))
            {
                AttackDistance = Unit.Parameters.StartAttackDistance + TargetUnit.Parameters.Radius;
            }
        }
    }
}