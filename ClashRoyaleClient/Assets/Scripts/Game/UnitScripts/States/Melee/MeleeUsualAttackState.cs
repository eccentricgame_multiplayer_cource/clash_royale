using TowerScripts;
using UnityEngine;

namespace UnitScripts.States.Melee
{
    [CreateAssetMenu(fileName = "_MeleeUsualAttackState", menuName = "UnitState/Melee/UsualAttackState")]
    public class MeleeUsualAttackState : AttackState
    {
        protected override void TryFindTarget()
        {
            TryAssignTargetAsWalkingUnit();
            TryAssignTargetAsTower();
        }

        private void TryAssignTargetAsWalkingUnit()
        {
            if (_hasTarget)
                return;

            bool hasNearestUnit = _mapInfo.TryGetNearestWalkingUnit(_unitPosition, _targetIsEnemy,
                out Unit nearestUnit, out float distance);

            if (hasNearestUnit && distance - nearestUnit.Parameters.Radius <= Unit.Parameters.StartAttackDistance)
            {
                _target = nearestUnit.Health;
                _stopAttackDistance = Unit.Parameters.StopAttackDistance + nearestUnit.Parameters.Radius;
                _hasTarget = true;
            }
        }

        private void TryAssignTargetAsTower()
        {
            if (_hasTarget)
                return;

            Tower nearestTower = _mapInfo.GetNearestTower(_unitPosition, _targetIsEnemy);
            if (nearestTower.GetDistance(_unitPosition) <= Unit.Parameters.StartAttackDistance)
            {
                _target = nearestTower.Health;
                _stopAttackDistance = Unit.Parameters.StopAttackDistance + nearestTower.Radius;
                _hasTarget = true;
            }
        }
    }
}