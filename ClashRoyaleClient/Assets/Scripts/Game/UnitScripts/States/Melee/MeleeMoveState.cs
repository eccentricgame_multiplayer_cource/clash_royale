using UnityEngine;

namespace UnitScripts.States.Melee
{
    [CreateAssetMenu(fileName = "_MeleeMoveState", menuName = "UnitState/Melee/MoveState")]
    public class MeleeMoveState : MoveState
    {
        protected override void MovingProcess()
        {
            TryAttackTargetTower();
            TryChaseWalkingUnit();
        }

        private void TryAttackTargetTower()
        {
            if (_targetTower == null)
                SetDefaultState();

            float distanceToTower = _targetTower.GetDistance(CurrentPosition);
            if (distanceToTower <= _startAttackDistance)
            {
                SetAttackState();
                Debug.Log($"Class: MoveState, methods: TryAttackTower is invoked");
            }
        }

        private void TryChaseWalkingUnit()
        {
            bool hasNearestUnit = _mapInfo.TryGetNearestWalkingUnit(
                CurrentPosition,
                _targetIsEnemy,
                out Unit nearestUnit,
                out float distance);

            if (!hasNearestUnit)
                return;

            if (distance <= _startChaseDistance)
            {
                SetChaseState();
                Debug.Log($"Class: MoveState, methods: TryAttackUnit is invoked");
            }
        }
    }
}