using UnityEngine;

namespace UnitScripts.States.Melee
{
    [CreateAssetMenu(fileName = "_MeleeChaseState", menuName = "UnitState/Melee/ChaseState")]
    public class MeleeChaseState : ChaseState
    {
        protected override void FindTarget()
        {
            if (MapInfo.TryGetNearestWalkingUnit(CurrentPosition, TargetIsEnemy, out TargetUnit, out float distance))
            {
                AttackDistance = Unit.Parameters.StartAttackDistance + TargetUnit.Parameters.Radius;
            }
        }
    }
}