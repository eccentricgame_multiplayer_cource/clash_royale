using TowerScripts;
using UnityEngine;
using UnityEngine.AI;

namespace UnitScripts.States
{
    public abstract class MoveState : UnitState
    {
        private NavMeshAgent _agent;
        protected float _startAttackDistance;
        protected float _startChaseDistance;
        protected bool _targetIsEnemy;
        protected Tower _targetTower;
        protected MapInfo _mapInfo;

        protected override void Construct(Unit unit)
        {
            base.Construct(unit);
            _agent = Unit.GetComponent<NavMeshAgent>();
            if (_agent == null)
                Debug.LogError($"Unit {Unit.name} don't have component NvMeshAgent");

            _startAttackDistance = Unit.Parameters.StartAttackDistance;
            _startChaseDistance = Unit.Parameters.StartChaseDistance;

            _agent.stoppingDistance = _startAttackDistance;
            _agent.speed = Unit.Parameters.Speed;
            _agent.radius = Unit.Parameters.Radius;
            _targetIsEnemy = !Unit.Parameters.IsEnemy;
            _mapInfo = MapInfo.Instance;
        }

        public override void Init()
        {
            _targetTower = _mapInfo.GetNearestTower(CurrentPosition, _targetIsEnemy);
            _agent.SetDestination(_targetTower.transform.position);
        }

        public override void Finish()
        {
            _agent.SetDestination(Unit.transform.position);
        }

        public override void Run()
        {
            MovingProcess();
        }

        protected abstract void MovingProcess();

        // private void TryAttackTower()
        // {
        //     float distanceToTower = _targetTower.GetDistance(CurrentPosition);
        //     if (distanceToTower <= _startAttackDistance)
        //     {
        //         Unit.SetState(UnitStateType.Attack);
        //         Debug.Log($"Class: MoveState, methods: TryAttackTower is invoked");
        //     }
        // }

        // private void TryAttackUnit()
        // {
        //     bool hasNearestUnit = _mapInfo.TryGetNearestAnyUnit(
        //         CurrentPosition,
        //         _targetIsEnemy,
        //         out Unit nearestUnit,
        //         out float distance);
        //
        //     if (!hasNearestUnit)
        //         return;
        //
        //     if (distance <= _startChaseDistance)
        //     {
        //         Unit.SetState(UnitStateType.Chase);
        //         Debug.Log($"Class: MoveState, methods: TryAttackUnit is invoked");
        //     }
        // }
    }
}