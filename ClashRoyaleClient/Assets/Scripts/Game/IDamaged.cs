public interface IDamaged
{
    Health Health { get; }
}