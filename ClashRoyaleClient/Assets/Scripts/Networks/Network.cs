using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace Networks
{
    public class Network : MonoBehaviour
    {
        //[SerializeField] private string URL;



        #region Singleton
        public static Network Instance { get; private set; }
        private void Awake()
        {
            if (Instance)
            {
                Destroy(gameObject);
                return;
            }

            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        #endregion
    
        // private void Start()
        // {
        //     // StartRun(URL, TODO, Success, (_) => { Debug.LogError("Error!!!"); });
        // }

        public void Post(string url, Dictionary<string,string> data, Action<string> onSuccess, Action<string> onError) =>
            StartCoroutine(PostAsync(url, data,onSuccess, onError));

        private IEnumerator PostAsync(string url, Dictionary<string,string> data, Action<string> onSuccess, Action<string> onError = null)
        {
            // WWWForm form = new WWWForm();
            // form.AddField("Login", "Barsyk");
            //
            // IMultipartFormSection section = new MultipartFormDataSection("Login", "Barsyk");
            //
            // section
            //data.Add("login", "Barsyc");
            using UnityWebRequest www = UnityWebRequest.Post(url, data);
            
            yield return www.SendWebRequest();
            if (www.result != UnityWebRequest.Result.Success)
                onError?.Invoke(www.error);
            else
                onSuccess?.Invoke(www.downloadHandler.text);
        }

        private void Error(string e) => Debug.LogError(e);

        private void Success(string result) => Debug.Log(result);
    }
}