using UnityEngine;
using UnityEngine.UI;

namespace Networks.Authorizations
{
    public class AuthorizationUI : MonoBehaviour
    {

        [SerializeField] private Authorization _authorization;
        [SerializeField] private UIActivator _uiActivator;

        [SerializeField] private InputField _login;
        [SerializeField] private InputField _password;

        [SerializeField] private Button _signIn;
        [SerializeField] private Button _signUp;

        private void Awake()
        {
            _login.onEndEdit.AddListener(_authorization.SetLogin);
            _password.onEndEdit.AddListener(_authorization.SetPassword);
        
            _signIn.onClick.AddListener(SignInClick);
            _signUp.onClick.AddListener(SignUpClick);

            _authorization.Error += OnError;
        }

        private void SignUpClick()
        {
            _uiActivator.ShowRegisterCanvas();
        }

        private void SignInClick()
        {
            _signIn.gameObject.SetActive(false);
            _signUp.gameObject.SetActive(false);
            _authorization.SignIn();
        }

        private void OnError()
        {
            _signIn.gameObject.SetActive(true);
            _signUp.gameObject.SetActive(true);
        }
    }
}
