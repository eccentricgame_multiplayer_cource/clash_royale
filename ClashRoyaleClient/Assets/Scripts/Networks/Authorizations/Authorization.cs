using System;
using System.Collections.Generic;
using UnityEngine;

namespace Networks.Authorizations
{
    public class Authorization : MonoBehaviour
    {
        public event Action Error;

        private string _login;
        private string _password;
        private string url;
        private string _loginKey = "login";
        private string _passwordKey = "password";

        private bool LoginOrPasswordIsEmpty => string.IsNullOrEmpty(_login) || string.IsNullOrEmpty(_password);

        private void Start() =>
            url = URLLibrary.MAIN + URLLibrary.AUTHORIZATION;

        public void SetLogin(string login) =>
            _login = login;

        public void SetPassword(string password) =>
            _password = password;

        public void SignIn()
        {
            if (LoginOrPasswordIsEmpty)
            {
                ErrorMessage("Логин и/или пароль пустые");
                return;
            }

            Dictionary<string, string> data = new Dictionary<string, string>()
            {
                { _loginKey, _login },
                { _passwordKey, _password },
            };
            Network.Instance.Post(url, data, OnSuccess, OnError);
        }

        private void OnSuccess(string data)
        {
            string[] result = data.Split("|");
            if (result.Length < 2 || result[0] != "ok")
            {
                ErrorMessage("Ответ с сервера вот такой: " + data);
                return;
            }

            if (int.TryParse(result[1], out int id))
            {
                UserInfo.Instance.SetId(id);
                Debug.Log($"Успешный вход, ID = {id}");
            
            }
            else
                ErrorMessage($"Не удалось распарсить \"{result[1]}\" в INT. Полный ответ вот такой: {data}");
        }

        private void OnError(string obj)
        {
        }

        private void ErrorMessage(string error)
        {
            Debug.LogError(error);
            Error?.Invoke();
        }
    }
}