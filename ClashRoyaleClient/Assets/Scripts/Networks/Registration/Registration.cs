using System;
using System.Collections.Generic;
using UnityEngine;

namespace Networks.Registration
{
    public class Registration : MonoBehaviour
    {
        public event Action Error;
        public event Action Success;

        private string _login;
        private string _password;
        private string _confirmPassword;
        private string url;
        private string _loginKey = "login";
        private string _passwordKey = "password";
        private string _confirmPasswordKey = "confirmPassword";

        private bool LoginOrPasswordIsEmpty => string.IsNullOrEmpty(_login) || string.IsNullOrEmpty(_password) ||
                                               string.IsNullOrEmpty(_confirmPassword);

        private void Start() =>
            url = URLLibrary.MAIN + URLLibrary.REGISTRATION;

        public void SetLogin(string login) =>
            _login = login;

        public void SetPassword(string password) =>
            _password = password;

        public void SetConfrimPassword(string confirmPassword) =>
            _confirmPassword = confirmPassword;

        public void SignUp()
        {
            if (LoginOrPasswordIsEmpty)
            {
                ErrorMessage("Логин и/или пароль пустые");
                return;
            }

            if (_password != _confirmPassword)
            {
                ErrorMessage("Пароли не совпадают");
                return;
            }

            Dictionary<string, string> data = new Dictionary<string, string>()
            {
                { _loginKey, _login },
                { _passwordKey, _password },
            };
            Network.Instance.Post(url, data, OnSuccess, OnError);
        }

        private void OnSuccess(string data)
        {
            if (data != "ok")
            {
                ErrorMessage("Ответ с сервера вот такой: " + data);
                return;
            }
            Debug.Log($"Успешная регистрация");
            Success?.Invoke();
        }

        private void OnError(string obj)
        {
        }

        private void ErrorMessage(string error)
        {
            Debug.LogError(error);
            Error?.Invoke();
        }
    }
}