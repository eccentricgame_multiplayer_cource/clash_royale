using UnityEngine;
using UnityEngine.UI;

namespace Networks.Registration
{
    public class RegistrationUI : MonoBehaviour
    {
        [SerializeField] private Registration _registration;
        [SerializeField] private UIActivator _uiActivator;
    
    
        [SerializeField] private InputField _login;
        [SerializeField] private InputField _password;
        [SerializeField] private InputField _confirmPassword;

        [SerializeField] private Button _applyBtn;
        [SerializeField] private Button _signInBtn;

        private void Awake()
        {
            _login.onEndEdit.AddListener(_registration.SetLogin);
            _password.onEndEdit.AddListener(_registration.SetPassword);
            _confirmPassword.onEndEdit.AddListener(_registration.SetConfrimPassword);
        
            _applyBtn.onClick.AddListener(SignUpClick);
            _signInBtn.onClick.AddListener(SignInClick);
        
            _registration.Error += OnError;

            _registration.Success += (() => _signInBtn.gameObject.SetActive(true));
        }

        private void SignInClick()
        {
            _uiActivator.ShowAuthCanvas();
        }

        private void SignUpClick()
        {
            _applyBtn.gameObject.SetActive(false);
            _signInBtn.gameObject.SetActive(false);
            _registration.SignUp();
        }

        private void OnError()
        {
            _applyBtn.gameObject.SetActive(true);
            _signInBtn.gameObject.SetActive(true);
        }}
}
