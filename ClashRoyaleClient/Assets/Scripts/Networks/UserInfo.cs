using UnityEngine;

namespace Networks
{
    public class UserInfo : MonoBehaviour
    {
        #region Singleton

        public static UserInfo Instance { get; private set; }

        private void Awake()
        {
            if (Instance)
            {
                Destroy(gameObject);
                return;
            }

            Instance = this;
            DontDestroyOnLoad(gameObject);
        }

        #endregion


        public int ID { get; private set; } = 19;

        public void SetId(int id)
        {
            ID = id;
        }
    }
}