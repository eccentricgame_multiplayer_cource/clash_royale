import { Room, Client } from "colyseus";
import { Schema, type, MapSchema } from "@colyseus/schema";
import axios from "axios";
import { Library } from "../Library";

export class Player extends Schema {

}

export class State extends Schema {
    @type({ map: Player })
    players = new MapSchema<Player>();


    createPlayer(sessionId: string) {
        this.players.set(sessionId, new Player());
    }

    removePlayer(sessionId: string) {
        this.players.delete(sessionId);
    }
}

export class StateHandlerRoom extends Room<State> {
    maxClients = 2;
    playersDeck = new Map();

    onCreate (options) {
        console.log("StateHandlerRoom created!", options);

        this.setState(new State());

        this.onMessage("move", (client, data) => {
            console.log("StateHandlerRoom received message from", client.sessionId, ":", data);
        });
    }

    async onJoin (client: Client, data) {
        try {
            const response = await axios.post(Library.getDeckURI, {key: Library.phpKEY, userID: data.id});
            console.log(response.data);
            this.playersDeck.set(client.id, response.data);
        } catch (error) {
            console.log('Вылетела ошибка' + error)
        }
        //hgjkljjhkjhk
       
        this.state.createPlayer(client.sessionId);
    }

    onLeave (client) {


       if(this.playersDeck.has(client.id))
            this.playersDeck.delete(client.id);

        this.state.removePlayer(client.sessionId);
    }

    onDispose () {
    }

}
